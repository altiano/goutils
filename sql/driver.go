package sql

import "database/sql"

// Open .
func Open(driverName, dataSourceName string) *sql.DB {
	db, err := sql.Open(driverName, dataSourceName)

	if err != nil {
		panic(err)
	}

	return db
}
