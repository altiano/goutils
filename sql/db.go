package sql

import (
	"database/sql"
	"strings"
)

// DbExec .
func DbExec(db *sql.DB, query string) sql.Result {
	r, err := db.Exec(strings.TrimSpace(`
		CREATE TABLE IF NOT EXISTS stockConfigs (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			name TEXT NOT NULL UNIQUE
		);
	`))

	if err != nil {
		panic(err)
	}

	return r
}

// DbBegin .
func DbBegin(db *sql.DB) *sql.Tx {
	tx, err := db.Begin()

	if err != nil {
		panic(err)
	}

	return tx
}

// DbQuery .
func DbQuery(db *sql.DB, query string) *sql.Rows {
	rows, err := db.Query(strings.TrimSpace(query))

	if err != nil {
		panic(err)
	}

	return rows
}

// RowsScan .
func RowsScan(rows *sql.Rows, dest ...interface{}) {
	err := rows.Scan(dest...)
	if err != nil {
		panic(err)
	}
}
