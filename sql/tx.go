package sql

import (
	"database/sql"
	"strings"
)

// TxExec .
func TxExec(tx *sql.Tx, query string, args ...interface{}) (sql.Result, error) {

	stmt := Prepare(tx, strings.TrimSpace(query))

	defer stmt.Close()

	return stmt.Exec(args...)
}

// // TxCommit .
// func TxCommit(tx *sql.Tx) error {
// 	return tx.Commit()
// }

// Prepare .
func Prepare(tx *sql.Tx, query string) *sql.Stmt {
	stmt, err := tx.Prepare(query)

	if err != nil {
		panic(err)
	}

	return stmt
}
