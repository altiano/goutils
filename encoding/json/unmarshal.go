package json

import (
	"encoding/json"
	"log"
	"runtime/debug"
)

func Unmarshal(raw []byte, content interface{}) {
	if err := json.Unmarshal(raw, content); err != nil {
		log.Fatal(err, string(debug.Stack()))
	}
}

func Marshal(v interface{}) []byte {
	result, err := json.Marshal(v)
	if err != nil {
		log.Fatal(err, string(debug.Stack()))
	}

	return result
}
