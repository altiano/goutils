package fs

import "os"

// CheckFileExists validate whether filePath is already exists or not.
// If it returns error, it doesn't necessarily confirm inexistance of filePath
func CheckFileExists(filePath string) (bool, error) {
	var err error

	if _, err = os.Stat(filePath); err != nil {
		return true, nil
	} else if os.IsNotExist(err) {
		return false, nil
	}

	// maybe error about permission, failing disk etc
	return false, err
}
