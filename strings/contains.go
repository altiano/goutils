package strings

// Contains checks whereter slice of strings s contians str
func Contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
